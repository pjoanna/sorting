package sort.strategies;
// kubełkowe - zlicza powtórzenia liczb które pojawiły się w tablicy
import sort.AbstractSortStrategy;
import sort.PorownywarkaAlgorytmow;

import java.util.HashMap;
import java.util.Map;

public class BucketSort extends AbstractSortStrategy {
    public BucketSort() {
        super();
    }

    public void sort() {
        int n = array.length;
        int[] kubelki  = new int[PorownywarkaAlgorytmow.getProgWartosci()];
        for (int i = 0; i < array.length; i++) {
            kubelki[array[i]]++;
        }

        int indexLiczb = 0;
        for (int i = 0; i < PorownywarkaAlgorytmow.getProgWartosci(); i++) {

            for (int j = 0; j < kubelki[i]; j++) {
               array[indexLiczb++] = i;
            }
        }
        System.out.println("Posortowano liczb " + indexLiczb);



     }
}

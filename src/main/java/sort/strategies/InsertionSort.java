package sort.strategies;

import sort.AbstractSortStrategy;

public class InsertionSort extends AbstractSortStrategy {

    public InsertionSort() {
        super();
    }

    public void sort() {
        // TODO: sortowanie insertion sort
        insertion(array);
    }

    public void insertion(Integer[] tablica){
        for (int i = 0; i < tablica.length; i++) {
            for (int j = i; j > 0; j--) {
                if (tablica[j] < tablica[j-1]){
                    int tmp = tablica[j];
                    tablica[i] = tablica[j];
                    tablica[j] = tmp;
                }
            }
        }
    }
}

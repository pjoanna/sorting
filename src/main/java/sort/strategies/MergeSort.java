package sort.strategies;

import sort.AbstractSortStrategy;

public class MergeSort extends AbstractSortStrategy {
    public MergeSort() {
        super();
    }

    public void sort() {
        mergeSort(array, 0, array.length-1);
    }

    public void mergeSort(Integer[] tablica, int first, int last) {
        if (first != last) {
            int middle = (first + last) / 2;
            mergeSort(tablica, first, middle); //lewo
            mergeSort(tablica, middle+1, last); //prawo
        }

    }

    public void merge(Integer[] tablica, int first, int middle, int last) {
        if (first!=last) {
            Integer[] tmp = new Integer[tablica.length];
            for (int i = 0; i < tablica.length; i++) {
                tmp[i] = tablica[i];
            }

            int i1 = first;
            int i2 = middle + 1;
            int obecnyIndex = first;
            while (i1 <= middle && i2 <= last) {
                if (tmp[i1] > tmp[i2]) {
                    tablica[obecnyIndex] = tmp[i2];
                    i2++;
                } else {
                    tablica[obecnyIndex] = tmp[i1];
                    i1++;
                }
                obecnyIndex++;
            }
            for (int i = i1; i <= middle; i++) {
                tablica[obecnyIndex++] = tmp[i];
            }
            for (int i = i2; i <= last; i++) {
                tablica[obecnyIndex++] = tmp[i];
            }
        }
    }

}


package sort.strategies;

import sort.AbstractSortStrategy;

public class BubbleSort extends AbstractSortStrategy {

    public BubbleSort() {
        super();
    }

    public void sort() {
        int n = array.length;
        do {
            for (int i = 0; i < n-1 ; i++) {
                if (array[i] > array[i+1]) {
                    int tmp = array[i + 1];
                    array[i + 1] = array[i];
                    array[i] = tmp;
                    n--;
                }
            }
        }while (n>1);
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
